# PHP PRACTICAL ASSESSMENT
> this project is designed as a practical assessment of your skill level, whilst there are instructions given, there is no set format or structure, as it will be up to you to design, architect and create your own project. Please note that you have 5days to complete the assessment Due date: 29 May 2020 @12h00pm .You are welcome to use a boilerplate or template to set up the project, but it is expected that you write the solution yourself, to the best of your ability.

## THE PROJECT
I had 5 days to build something that will showcase my drupal web development skill.
I decided to find a website online to redesign and develop using drupal 8. The existing website I found was this wordpress site (https://www.xilumaniguesthouseandspa.com).

## EXTRAS
If i had time i would include:
- A booking systerm for rooms reservation.
- Multilingual to translate the site to xitsonga
- Content Moderation for content reviews
I couldn't find a video of  the guest house on youtube, so i just used my own video for demo purpose.
I got the pictures i used from theire facebook page and website, but some i downloaded from https://unsplash.com

## MODULES USED
- Paragraphs
- Owlcarousel
- Webform
- Video embed field
- Ctools
- Colourbox
- entity reference revisions

## HOW TO GET STARTED

### DDEV CONFIG
1. you need to have ddev and docker to run this project on your local machine
2. Clone the repo to your local machine `git clone https://gitlab.com/ikeymarilele/xilumani-guest-house.git`
3. go to your peojeect and run `ddev config`
4. run `ddev start`  (this will also gives you a URL to the website )
5. run `ddev ssh`
6. run `composer install` 
7. once composer install is finish run `exit` 
8. Import the database you can run `ddev import-db --src=./DB/db_2020-05-29.sql.gz`

### NPM
1. npm install
2. Now go to the custom theme (wtthemes) and run the following
    - npm install gulp -g
    - npm init
    - npm install gulp --save-dev
3. To watch the sass use `gulp sass`
4. To clean the css and build styles, fonts and scripts use `gulp`


### DRUPAL LOGIN
drush uli